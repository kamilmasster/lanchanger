﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace LanChanger
{
    public partial class Form1 : Form
    {

        private DataSet mySqlData;
        string[] ifaces;
        int i;
        string selected;
        bool start;
        int selectedIp, selectedCard;
        string username;
        bool syrion, dhcp;

        public Form1()
        {
            InitializeComponent();
            this.Size = new Size(440, 240);
            textBox1.Text = "192.168.12";
            textBox2.Text = "255.255.255.0";
            textBox3.Text = "192.168.12";
            textBox4.Text = "";
            label1.Text = "";
            ifaces = new string[20];
            i = 0;
            selected = "";
            textBox5.Enabled = false;
            start = true;
            this.Text = System.Environment.MachineName;
            username = System.Environment.MachineName;
            this.WindowState = FormWindowState.Minimized;
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                //notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
            this.ShowInTaskbar = false;

            mySqlData = new DataSet();



            listBox1.Items.Clear();
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    listBox1.Items.Add(objMO["Caption"].ToString());
                }
            }
            if (System.Environment.MachineName == "P4100035" || System.Environment.MachineName == "4100227" || System.Environment.MachineName == "P4100515")
            {
                button5.Enabled = true;
            }
            else
            {
                button5.Enabled = false;
            }

            timer1.Enabled = true;


            string IP = Dns.GetHostEntry(Dns.GetHostName()).AddressList.First(f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();

            if (IP.Substring(8, 3) == "125")
            {
                syrion = true;
                dhcp = false;
            }
            else
            {
                syrion = false;
                dhcp = true;
            }

        }

        private void zmienIP(string newIP, string subnetMask, string gateway, string dns1, bool manual)
        {

            if (selected != "")
            {

                if (newIP != "")
                {
                    try
                    {
                        if (manual)
                        {
                            dbConnect.Query("UPDATE `lanChanger` SET `time`=NOW() ,`user` ='" + Environment.MachineName + "' ,`use`='Y' where id = " + (selectedIp + 1).ToString());
                            dbConnect.Query("INSERT INTO `lanChanger_log`(`id_pc`, `od`, `do`, `ip`) VALUES ('" + Environment.MachineName + "',NOW(),'','" + newIP + "')");
                        }
                        setIP(selected, newIP, subnetMask);
                        setGateway(selected, gateway);
                        setDNS(selected, dns1);
                        dhcp = false;
                        syrion = true;
                        MessageBox.Show("IP Changed Successfully");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
                else
                {
                    MessageBox.Show("ERROR!");
                }
            }
            else
            {
                MessageBox.Show("nie wybrales karty sieciowej");
            }

        }

        private void setIP(string NIC, string ip_address, string subnet_mask)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    if (objMO["Caption"].Equals(NIC))
                    {
                        ManagementBaseObject setIP;
                        ManagementBaseObject newIP = objMO.GetMethodParameters("EnableStatic");
                        newIP["IPAddress"] = new string[] { ip_address };
                        newIP["SubnetMask"] = new string[] { subnet_mask };
                        setIP = objMO.InvokeMethod("EnableStatic", newIP, null);
                    }
                }
            }
        }

        private void setGateway(string NIC, string gateway)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    if (objMO["Caption"].Equals(NIC))
                    {
                        ManagementBaseObject setGateway;
                        ManagementBaseObject newGateway = objMO.GetMethodParameters("SetGateways");

                        newGateway["DefaultIPGateway"] = new string[] { gateway };
                        newGateway["GatewayCostMetric"] = new int[] { 1 };

                        setGateway = objMO.InvokeMethod("SetGateways", newGateway, null);
                    }
                }
            }
        }

        private void setDNS(string NIC, string DNS)
        {
            ManagementClass objMC = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection objMOC = objMC.GetInstances();

            foreach (ManagementObject objMO in objMOC)
            {
                if ((bool)objMO["IPEnabled"])
                {
                    if (objMO["Caption"].Equals(NIC))
                    {
                        ManagementBaseObject newDNS = objMO.GetMethodParameters("SetDNSServerSearchOrder");
                        newDNS["DNSServerSearchOrder"] = DNS.Split(',');
                        ManagementBaseObject setDNS = objMO.InvokeMethod("SetDNSServerSearchOrder", newDNS, null);
                    }
                }
            }
        }

        private void setDHCP(string NIC)
        {
            if (!dhcp) { 
            var adapterConfig = new ManagementClass("Win32_NetworkAdapterConfiguration");
            var networkCollection = adapterConfig.GetInstances();

            foreach (ManagementObject adapter in networkCollection)
            {
                try
                {
                    if (adapter["Caption"].Equals(NIC))
                    {

                        dbConnect.Query("UPDATE `lanChanger` SET `use`='N',`user`='',`time`='' where id = " + (selectedIp + 1).ToString());
                        dbConnect.Query("UPDATE `lanChanger_log` SET `do`=NOW() WHERE `id_pc`='" + Environment.MachineName + "'");
                        adapter.InvokeMethod("EnableDHCP", null);
                        MessageBox.Show("Updated Dynamic address!");
                            dhcp = true;
                            syrion = false;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to Set IP : " + ex.Message);
                }
            }

        }
    }

        private void textBox2_TextChanged(object sender, MouseEventArgs e)
        {
            textBox2.Text = "255.255.255.0";
        } //mask

        private void button4_Click(object sender, EventArgs e)
        {

            if (selected != "")
            {
                setDHCP(selected);
                setDNS(selected, "172.100.100.11");
            }
            else
            {
                MessageBox.Show("nie wybrales karty sieciowej");
            }



        } //DHCP

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selected = listBox1.SelectedItem.ToString();
            selectedCard = listBox1.SelectedIndex;
        } //wybor sieciowki

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        } //tray 

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (start)
            {
                notifyIcon1.BalloonTipTitle = "App started in tray";
                notifyIcon1.BalloonTipText = "You have successfully minimized your form.";
                start = false;
            }
            else
            {
                notifyIcon1.BalloonTipTitle = "Minimize to Tray App";
                notifyIcon1.BalloonTipText = "You have successfully minimized your form.";
            }
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
                this.ShowInTaskbar = true;
                this.Show();
            }
        } //resize

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Dispose();
            this.Close();
        } //exit

        private void timer1_Tick(object sender, EventArgs e) //timer
        {
            comboBox1.SelectedIndex = -1;
            comboBox1.Items.Clear();
            label2.Text = dbConnect.returnConnectionState();
            try
            {

                label1.Text = dbConnect.retIP();
                if (dbConnect.retIP() != "127.0.0.1")
                {
                    dbConnect.Connect();
                    if (dbConnect.retConState() == ConnectionState.Open)
                    {
                        mySqlData = dbConnect.Query("SELECT * FROM `lanChanger` WHERE `use`='N'");
                        foreach (DataTable dt in mySqlData.Tables)
                        {
                            foreach (DataRow dataRow in dt.Rows)
                            {
                                if (System.Environment.MachineName == "P4100035" && i == 0) { comboBox1.Items.Add(dataRow[0] + " " + dataRow[1] + " " + dataRow[4]); }
                                else if (i > 0) { comboBox1.Items.Add(dataRow[0] + " " + dataRow[1] + " " + dataRow[4]); }
                            }
                        }
                        dbConnect.Close();
                    }
                    else
                    {
                        timer1.Enabled = false;
                        MessageBox.Show("Brak polaczenia z baza!" + Environment.NewLine + dbConnect.status + Environment.NewLine + dbConnect.retConState().ToString());
                    }
                    label2.Text = GetIP();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }
        public string GetIP()
        {
            string externalIP = "";
            externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
            externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")).Matches(externalIP)[0].ToString();
            return externalIP;
        }

        private void button5_Click(object sender, EventArgs e) //manual IP show/hide
        {
            if (this.Size.Width == 640) this.Size = new Size(440, 240);
            else this.Size = new Size(640, 240);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            zmienIP(textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text, false);
        } //button zmien IP manual

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dhcp)
            {
                if (comboBox1.SelectedIndex != -1)
                {
                    timer1.Enabled = false;
                    dbConnect.Close();
                    selectedIp = comboBox1.SelectedIndex;
                    try
                    {
                        if (!syrion)
                        {
                            dbConnect.Connect();
                            if (dbConnect.retConState() == ConnectionState.Open)
                            {
                                string tempIP;
                                tempIP = mySqlData.Tables[0].Rows[selectedIp][1].ToString();
                                zmienIP(tempIP, "255.255.255.0", "192.168.125.254", "109.196.48.2", true);
                                dbConnect.Close();
                                
                            }
                        }
                        else
                        {
                            MessageBox.Show("Masz już IP z Syrionu!");
                        }
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    timer1.Enabled = true;
                }
            }
        }
    }
}