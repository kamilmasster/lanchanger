﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LanChanger
{
    static class dbConnect
    {
        static public string status;
        static public MySqlConnection Connection { get; set; } = null;

        static private string Server;
        static private string Database;
        static private string UserName;
        static private string Password;
        static private string connectionString;
        static private ConnectionState connectionState;
        static private string IP;

        static public string returnConnectionString()
        {
            return connectionString;
        }
        static public string returnConnectionState()
        {
            return connectionState.ToString();
        }
        static public ConnectionState retConState()
        {
            return connectionState;
        }
        static public string retIP()
        {
            IP = Dns.GetHostEntry(Dns.GetHostName()).AddressList.First(f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();
            return IP;
        }



        static public void Connect()
        {

            IP = Dns.GetHostEntry(Dns.GetHostName()).AddressList.First(f => f.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).ToString();


            if (Server == null || Database == null || UserName == null || Password == null)
            {
                Server = "192.168.120.241";
                Database = "Config";
                UserName = "lanchanger";
                Password = "slpoland1";
            }
            if (IP.Contains("125")) Server = "192.168.125.250";
            else Server = "192.168.120.241";


            connectionString = "SERVER=" + Server + ";" + "DATABASE=" + Database + ";" + "UID=" + UserName + ";" + "PASSWORD=" + Password + ";";

            try
            {
                Connection = new MySqlConnection(connectionString);
                Connection.ConnectionString = connectionString;
                Connection.Open();
                connectionState = Connection.State;
            }
            catch (MySqlException ex)
            {
                status = ex.ToString();
                connectionState = Connection.State;
            }

        }

        static public void Close()
        {
            if (Connection.State == ConnectionState.Open)
            {
                Connection.Close();
                Connection.Dispose();
            }
        }
        static public DataSet Query(string query)
        {
            DataSet ds = new DataSet();
            try
            {
                using (Connection)
                {
                    using (MySqlDataAdapter adapter = new MySqlDataAdapter(query, Connection))
                    {
                        adapter.Fill(ds);

                    }
                }
            }
            catch (Exception ex)
            {

            }
            return ds;
        }
        static public DataTable Query1(string query)
        {

            DataTable output = new DataTable();
            try
            {

                if (connectionState == ConnectionState.Open)
                {
                    MySqlCommand cmd = new MySqlCommand(query, Connection);
                    cmd.ExecuteNonQuery();
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(output);
                }


            }
            catch (Exception ex)
            {

            }
            return output;


        }


    }
}
